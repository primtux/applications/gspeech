# Gspeech

Interface graphique au synthétiseur vocal Svoxpico.
Permet de lire le contenu du presse-papiers ou d'un texte sélectionné, de mettre pause, gérer la vitesse de lecture etc.